<?php
/**
 * Utility functions
 */
if(!function_exists('add_filters')){
	function add_filters($tags, $function) {
		foreach($tags as $tag) {
		  add_filter($tag, $function);
		}
	}
}
if(!function_exists('is_element_empty')){
	function is_element_empty($element) {
	  $element = trim($element);
	  return empty($element) ? false : true;
	}
}