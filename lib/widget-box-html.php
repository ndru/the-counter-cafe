<?php 
/**
 * HTMLBox widget class
 *
 * Copied from the Text widget, to allow for custom html ID and CLASS attributes
 *
 * @since 2.8.0
 */
 
 function htmlbox_widget_init() {
  	register_widget('Counter_Widget_HTMLBox');
 }
 add_action('widgets_init', 'htmlbox_widget_init');

 
class Counter_Widget_HTMLBox extends WP_Widget {

	function Counter_Widget_HTMLBox() {
		$widget_ops = array('classname' => 'widget_html', 'description' => __('Arbitrary HTML'));
		$control_ops = array('width' => 400, 'height' => 350);
		$this->WP_Widget('htmlbox', __('HTML Box'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$text = apply_filters( 'widget_text', $instance['text'], $instance );
		//echo $before_widget;
?>
			<?php echo $text; ?>
		<?php
		//echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( current_user_can('unfiltered_html') ){
			$instance['text'] =  $new_instance['text'];
		}else{
			$instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
		}
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = strip_tags($instance['title']);
		$element = !empty($instance['element']) ? $instance['element'] : 'div';
		$class = strip_tags($instance['class']);
		$id = strip_tags($instance['id']);
		$text = esc_textarea($instance['text']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>
<?php
	}
}
