<?php
/**
 * Custom functions
 */
require(__DIR__.'/widget-box-html.php');

/**
 * Register widgetized area and update sidebar with default widgets
 */
function thecountercafe_widgets_init() {

   register_sidebar(array(
       'name'=>'Navigation Widgets',
       'id'=>'nav-widgets',
       'before_widget' => '<div id="%1$s" class="nav-widget %2$s">',
       'after_widget' => '</div>',
       'before_title' => '<h2 class="widget-title">',
       'after_title' => '</h2>',
   ));

}
add_action( 'init', 'thecountercafe_widgets_init' );


function counter_get_bg() {

	global $wp_query;
	if(is_home()){
		$thePostID = get_option('page_for_posts');
	}else{
		$thePostID = get_the_ID();
	}
	$bg = wp_get_attachment_image_src( get_post_thumbnail_id($thePostID), 'fullsize');
  $bg_small = wp_get_attachment_image_src( get_post_thumbnail_id($thePostID), 'large');
	return array($bg[0], $bg_small[0]);
}

if(class_exists('WP_Customize_Control')){

class Counter_Customize_Textarea_Control extends WP_Customize_Control {
    public $type = 'textarea';
 
    public function render_content() {
        ?>
        <label>
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
        </label>
        <?php
    }
}

}


function counter_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'counter.header' , array(
	    'title'      => __( 'Site Header', 'countercafe' ),
	    'priority'   => 30,
	) );
	
	
	//set the site meta-description tag
	$wp_customize->add_setting( 'counter[meta-description]' , array(
				'transport'   => 'refresh',
				'default'			=> ''
	));
	$wp_customize->add_control(new Counter_Customize_Textarea_Control( $wp_customize, 'counter[meta-description]', array(
			'label'    => __('Meta Description', 'countercafe'),
			'section'  => 'counter.header',
			'priority' => 1
	) ));
	

	/*$wp_customize->add_setting( 'counter[metric]' , array(
	    'default'     => 'flamingo_pier',
	    'transport'   => 'refresh',
	) );
	$wp_customize->add_control( 'counter[metric_options]', array(
	    'settings' => 'counter[metric]',
	    'label'   => __('Display metric...', 'countercafe'),
	    'section' => 'counter',
	    'type'    => 'select',
	    'choices'    => array(
	        'flamingo_pier' => __('Days until next Flamingo Pier', 'countercafe'),
	        'coffee_roasted' => __('Kgs of coffee roasted', 'countercafe'),
	    ),
	));*/
	
	//toggle header animation
	$wp_customize->add_setting( 'counter[animation_enabled]' , array(
	    'transport'   => 'refresh',
	    'default'			=> 1
	));
	$wp_customize->add_control('counter[animation_enabled]', array(
    'label'    => __('Animate header?', 'countercafe'),
    'section'  => 'counter.header',
    'type'     => 'checkbox',
    'priority'   => 20,
	));
	
	//set the header size
	$wp_customize->add_setting( 'counter[header_height-xs]' , array(
		'transport'   => 'refresh',
		'default'			=> 400,
		'priority'   => 30,
));
	$wp_customize->add_control('counter[header_height-xs]', array(
		'label'    => __('Header height on mobile', 'countercafe'),
		'section'  => 'counter.header',
		'type'		 => 'text'
	 ));
	$wp_customize->add_setting( 'counter[header_height-md]' , array(
		'transport'   => 'refresh',
		'default'			=> 400,
		'priority'   => 40,
	));
	$wp_customize->add_control('counter[header_height-md]', array(
		'label'    => __('Header height on tablet and desktop', 'countercafe'),
		'section'  => 'counter.header',
		'type'		 => 'text'
	 ));
	 

	//set the address
	$wp_customize->add_setting( 'counter[address]' , array(
	    'transport'   => 'refresh',
	    'default'			=> 20
	));
	$wp_customize->add_control(new Counter_Customize_Textarea_Control( $wp_customize, 'counter[address]', array(
	    'label'    => __('Address', 'countercafe'),
	    'section'  => 'counter.header'
	) ));



	//toggle the flamingo pier countdown
	$wp_customize->add_setting( 'counter[flamingo_countdown_enabled]' , array(
	    'transport'   => 'refresh'
	));
	$wp_customize->add_control('counter[flamingo_countdown_enabled]', array(
    'label'    => __('Enable automatic countdown to Flamingo Pier?', 'countercafe'),
    'section'  => 'counter.header',
    'type'     => 'checkbox'
	));


	//set the amount of coffee roasted per day
	$wp_customize->add_setting( 'counter[coffee_roasted]' , array(
	    'transport'   => 'refresh',
	    'default'			=> 20
	));
	$wp_customize->add_control('counter[coffee_roasted]', array(
    'label'    => __('Kilograms of Coffee roasted per day...', 'countercafe'),
    'section'  => 'counter.header',
    'type'		 => 'text'
   ));
   
   
   //set the number of eggs poached each day
   $wp_customize->add_setting( 'counter[eggs_poached]' , array(
   		'transport'   => 'refresh',
   		'default'			=> 80
   	));
  $wp_customize->add_control('counter[eggs_poached]', array(
   	'label'    => __('Avg. number of eggs poached each day...', 'countercafe'),
   	'section'  => 'counter.header',
   	'type'		 => 'text'
  ));




}
add_action( 'customize_register', 'counter_customize_register' );
