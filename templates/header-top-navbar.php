<?php

if(is_page()){
	if ($post->post_parent)	{
		$ancestors=get_post_ancestors($post->ID);
		$root=count($ancestors)-1;
		$page = $ancestors[$root];
	} else {
		$page = $post->ID;
	}
	$pagename = get_post($page)->post_name;
}

$opts = get_theme_mod('counter');
?>

<style>
header.banner{
  <?php if(isset($opts['header_height-xs'])): ?>height:<?php echo $opts['header_height-xs'];?>px;<?php endif; ?>
}
header.banner #primary-navigation{
  <?php if(isset($opts['header_height-xs'])): ?>top:<?php echo $opts['header_height-xs']+30;?>px;<?php endif; ?>
}
@media (min-width: 768px){
  header.banner{
    <?php if(isset($opts['header_height-xs'])): ?>height:<?php echo $opts['header_height-md']; ?>px;<?php endif; ?>
  }
  header.banner #primary-navigation{
    <?php if(isset($opts['header_height-xs'])): ?>top:<?php echo $opts['header_height-md']+30;?>px;<?php endif; ?>
  }
}

</style>
<header class="banner" role="banner">


  <div id="page-banner">
      <div class="logo">
        <a class="brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
      </div>
    <div class="logo-background">
      <!-- no transitions/animations on psuedo elements in webkit, so we're using empty divs -->
      <div class="waves"></div>
      <div class="circle"></div>
    </div>
  </div>

  <div id="address">
  <?php $mod = get_theme_mod('counter'); echo $mod['address']; ?>
  </div>

  <div id="primary-navigation">

    <div class="container">
      <nav class="" id="top-nav" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => ''));
          endif;
        ?>
      </nav>
    <?php
if(is_page()):
    if (has_nav_menu($pagename)) : ?>
      <nav class="" id="page-nav">
      <?php wp_nav_menu(array('theme_location' => $pagename, 'menu_class' => '')); ?>
      </nav>
    <?php endif; ?>
<?php endif; ?>
    </div>
  </div>

<?php
    $return = '';

    //print_r($opts);

   //calculate the days to the next first-friday-of-the-month

		//first friday in the current month
		$first_friday  = strtotime('first fri of this month');

		if($first_friday < time()){
			//already gone...

			//number of days remaining in the month
			$days_remaining = date('t')-date('j');
			//number of days until first friday of next month
			$next = $days_remaining + date('j', strtotime('first fri of next month'));
		}else{
			//number of days til the first friday of this month
			$next = date('j', $first_friday) - date('j');
		}

    $max_scale = max(4+$next, 7);


		//echo date('d m Y', strtotime('2014-09-20 first fri of this month'));
?>
<?php
 if( $opts['flamingo_countdown_enabled']==true && $next < 8):  ?>
  <div id="measures" class="flamingo_pier">
      <div class="lines"></div>
    <div class="unit"><span class="left">days to go...</span><span class="right">...until</span></div>
    <ul class="scale" style="top:-<?php echo ($max_scale - $next)*25 + 25 ?>px">
      <?php for($i=$max_scale;$i>0;$i--){
        echo '<li>'.$i.'</li>';
        }?>
    </ul>
    <a class="reference" href="/cafe/flamingo-pier/">
    </a>
  </div>
  <?php else:
    if( $pagename === 'cafe' ):
      $max_scale = 10;
      $per_day = $opts['eggs_poached'] ?: 80;
      $today =  date('w')+1;
      $scale = $max_scale - $today; //(date('w')+1) * $per_day;
      ?>
      <div id="measures" class="eggs_poached">
        <div class="lines"></div>
        <div class="unit"><span class="left">Eggs poached...</span> <span class="right">...this week</span></div>
        <ul class="scale" style="top:-<?php echo $scale * 25 + 25 ?>px">
          <?php for($i=$max_scale;$i>0;$i--){
            echo '<li>'.($i*($per_day)).'</li>';
            }?>
        </ul>
        <div class="reference">
        </div>
      </div>
      <?php
    else:
      $max_scale = 10;
      $per_day = $opts['coffee_roasted'] ?: 2;
      $today =  date('w')+1;
      $scale = $max_scale - $today; //(date('w')+1) * $per_day;
    ?>
    <div id="measures" class="coffee_roasted">
      <div class="lines"></div>
      <div class="unit"><span class="left">Kg of coffee...</span> <span class="right">...roasted this week</span></div>
      <ul class="scale" style="top:-<?php echo $scale * 25 + 25 ?>px">
        <?php for($i=$max_scale;$i>0;$i--){
          echo '<li>'.($i*($per_day)).'</li>';
          }?>
      </ul>
      <div class="reference">
      </div>
    </div>
  <?php 
    endif;
  endif;?>

</header>
