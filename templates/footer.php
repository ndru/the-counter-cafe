<footer class="content-info" role="contentinfo">
  <div class="container">
  <?php $page = $wp_query->query_vars['pagename']; 
  if($page ==='cafe' || $page==='roastery'): ?>
		<?php $social = $page==='cafe' ? 'countercafe' : 'counterroastery'; ?>
		<nav class="social">
			<ul>
				<li><a href="http://facebook.com/<?php echo $social ?>" target="_blank" rel="tooltip" title="<?php echo $social?>"><i class="fa fa-facebook"></i></a></li>
				<li><a href="http://twitter.com/<?php echo $social ?>" target="_blank" rel="tooltip" title="@<?php echo $social ?>"><i class="fa fa-twitter"></i></a></li>
				<li><a href="http://instagram.com/<?php echo $social ?>" target="_blank" rel="tooltip" title="@<?php echo $social ?>"><i class="fa fa-instagram"></i></a></li>
			</ul>
		</nav>
<?php endif; ?>
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
  </div>
</footer>

<?php wp_footer(); ?>
