<?php get_template_part('templates/head'); ?>
<?php 
$opts = get_theme_mod('counter');

$animated = !empty($opts['animation_enabled']) ? 'animated' : '';

switch(basename(roots_template_path(), '.php')){
  case 'template-light':
    $variant = 'light';
  break;
  default:
    $variant = 'dark';
  break;
} ?>


<?php 

//Used to detect the parent page and show the right submenu even on subpages

if(is_page()){
	if ($post->post_parent)	{
		$ancestors=get_post_ancestors($post->ID);
		$root=count($ancestors)-1;
		$page = $ancestors[$root];
	} else {
		$page = $post->ID;
	}
	$pagename = get_post($page)->post_name;
	$top_page = $pagename; 
}else{
	$top_page= '';
}

?>


<body <?php body_class($variant.' '.$animated.' '.$top_page); ?>>
<?php
  //get custom parameters for page
  $custom = get_post_custom( get_option('page_for_posts') );

  $background_image = counter_get_bg();

  $show_title = isset($custom['Show title'][0]) ? $custom['Show title'][0] : false;
  $show_content = isset($custom['Show content'][0]) ? $custom['Show content'][0] : true;


?>
    <style type="text/css">
    body{
      background-image: url(<?php echo $background_image[0] ?>);
    }
    @media (min-width: 500px){
      body{
        background-image: url(<?php echo $background_image[1] ?>);
      }
    }
    </style>
  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    //if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    //} else {
    //  get_template_part('templates/header');
    //}

  $show_content = !empty($post->post_content);
  $custom = get_post_custom($post->ID);
  if( isset($custom['Show content'][0]) ){
    $show_content =  $custom['Show content'][0];
  }
  ?>
  
  <div class="wrap" role="document">
    <div class="content row" <?php /*if(!$show_content){ ?>style="display:none"<?php }*/ ?>>
      <main class="main <?php echo roots_main_class(); ?>" role="main">
        <?php include roots_template_path(); ?>
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->
  <?php get_template_part('templates/footer'); ?>

</body>
</html>
